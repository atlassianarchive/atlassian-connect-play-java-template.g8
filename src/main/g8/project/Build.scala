import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "atlassian-connect-play-java-template"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    "com.atlassian.connect" % "ac-play-java_2.10" % "0.6.2",
    javaCore,
    javaJdbc,
    javaEbean
    // Add your project dependencies here
  )

  val main = play.Project(appName, appVersion, appDependencies).settings(
    resolvers += "Atlassian's Maven Public Repository" at "https://maven.atlassian.com/content/groups/public",
    resolvers += "Local Maven Repository" at "file://" + Path.userHome + "/.m2/repository"
    // Add your own project settings here
  )

}
